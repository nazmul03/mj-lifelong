var express = require('express')
var app = express()
var path = require('path');

app.use(express.static(path.join(__dirname, './build')));

app.use(function(req, res, next) {
    if (req.url.match(/^\/(css|js|img|font)\/.+/)) {
        res.setHeader('Cache-Control', 'public, max-age=3600'); // cache header
    }
    next();
});

app.use(function(req, res) {
    res.sendfile(__dirname + '/dist/index.html');
});

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err);
});


var fs = require('fs');

console.log("Application started")

app.listen(8080, '0.0.0.0')