import React from 'react';
import { Switch, Route, Redirect, HashRouter } from 'react-router-dom';

import Footer from './containers/Footer';
import Event from './containers/Event';
import EmptyPage from './containers/EmptyPage';
import Thankyou from './containers/Thankyou';

function App() {
  const theme = {
    backgroundColor: '#F1F0F0',
    contentBackgroundColor: '#F1F0F0',
    headerTextColor: '#B82318',
    bodyTextColor: '#000000',
    boxBackgroundColor: '#B82318',
    boxTextColor: '#FFFFFF',
    mediaBoxBackgroundColor: '#F1F0F0'
  };

  return (
    <>
      <HashRouter>
        <div style={{ backgroundColor: theme.backgroundColor, height: '100vh', width: '100vw', overflowY: 'auto' }}>
          <Switch>
            <Route exact path='/' render={(props) => <EmptyPage {...props} theme={theme} />} />
            <Route exact path='/event/:eventId' render={(props) => <Event {...props} theme={theme} />} />
            <Route
              exact
              path='/event/:eventId/collaborator/:collaboratorId'
              render={(props) => <Event {...props} theme={theme} />}
            />
            <Route exact path='/thankyou/:eventId' render={(props) => <Thankyou {...props} theme={theme} />} />
            <Redirect to='/' />
          </Switch>
          <Footer theme={theme} />
        </div>
      </HashRouter>
    </>
  );
}

export default App;
