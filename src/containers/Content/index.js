import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import { isMobile } from 'react-device-detect';

import MediaOptions from '../MediaOptions';
import UserForm from '../UserForm';
import Progress from '../components/Progress';

const Content = (props) => {
  const [fileObj, setFileObj] = React.useState([]);
  const [user, setUser] = React.useState({
    event_collaborator: null,
    name: '',
    email: '',
    meta_data: {
      social_media: ''
    },
    collab_event_id: props && props.colabEventId
  });

  const [uploadProcessing, setUploadProcessing] = React.useState(false);

  const createColabMedia = (eventId, collaboratorId) => {
    // eslint-disable-next-line
    fileObj.map((file) => {
      let media = {
        ...file,
        thumbnail_url: null,
        collab_event: eventId,
        event_collaborator: collaboratorId
      };
      axios
        .post('https://api.miljulapp.com/api/v1/api/v1/collab/collabmedia', media)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            props.history.push({
              pathname: `/thankyou/${eventId}`,
              state: {
                message: props.data
                  ? props.data.meta_data
                    ? props.data.meta_data
                      ? props.data.meta_data.ty_callout
                        ? props.data.meta_data.ty_callout
                        : null
                      : null
                    : null
                  : null
              }
            });
          } else props.setMessage('Media could not be created');
        })
        .catch((error) => {
          console.log(error);
          props.setMessage('Media could not be created');
        });
    });
  };

  const createCollaborator = () => {
    if (fileObj.length > 0) {
      if (uploadProcessing === false) {
        if (props.collaboratorId) {
          createColabMedia(user.collab_event_id, user.event_collaborator);
        } else {
          let collaborator = { ...user, collab_event_id: props.colabEventId, collborator_type: 'OTHERS' };
          axios
            .post('https://api.miljulapp.com/api/v1/api/v1/collab/collaborators', collaborator)
            .then((response) => {
              console.log(response);
              if (response.status === 200) {
                createColabMedia(user.collab_event_id, response.data.response.response.id);
              } else props.setMessage('Collaborator could not be created');
            })
            .catch((error) => {
              console.log(error);
              props.setMessage('Collaborator could not be created');
            });
        }
      } else {
        props.setMessage('File uploading is not complete yet');
      }
    } else {
      props.setMessage('You have not added any file yet');
    }
  };

  React.useEffect(() => {
    if (props.collaboratorId) {
      axios
        .get(`https://api.miljulapp.com/api/v1/api/v1/collab/collaborators/${props.collaboratorId}`)
        .then((response) => {
          console.log(response);
          let data = response.data.response.response;
          setUser({
            ...user,
            event_collaborator: data.id || null,
            name: data.name || '',
            email: data.email || '',
            collborator_type: data.collborator_type || '',
            collab_event_id: props.colabEventId,
            meta_data: {
              social_media: data.meta_data ? (data.meta_data.social_media ? data.meta_data.social_media : '') : ''
            }
          });
        })
        .catch((error) => {
          console.log(error);
          props.setMessage('Collaborator could Found');
        });
    }
    // eslint-disable-next-line
  }, [props]);

  const { data, message, setMessage } = props;

  return (
    <div style={{ backgroundColor: props.theme.backgroundColor, padding: '15px 0' }}>
      {data ? (
        data.detail === 'Not found.' ? (
          <div style={{ minHeight: '70vh', position: 'relative' }}>
            <div style={{ position: 'absolute', top: '50%', left: '40%' }}>
              <h1 style={{ color: props.theme.headerTextColor }}>Sorry! Event Not Found</h1>
            </div>
          </div>
        ) : (
          <div
            style={{
              width: isMobile ? '90vw' : '60vw',
              margin: '0 auto',
              boxShadow: '0px 0px 5px 2px #ddd',
              padding: isMobile ? '1rem' : '1rem 2rem',
              borderRadius: '5px',
              backgroundColor: props.theme.contentBackgroundColor
            }}
          >
            <div
              style={{
                textAlign: 'center',
                fontSize: isMobile ? '1.25rem' : '2rem',
                fontWeight: '600',
                textJustify: 'auto',
                color: props.theme.headerTextColor
              }}
            >
              <span>{data.name ? data.name : ''}</span>
            </div>
            <div
              style={{ textAlign: 'center', fontSize: '2vmax', fontWeight: '600', color: props.theme.headerTextColor }}
            >
              {data.meta_data ? data.meta_data.callout || '' : ''}
            </div>
            <div
              style={{ textJustify: 'auto', padding: '1rem 0', fontSize: '0.9rem', color: props.theme.bodyTextColor }}
            >
              {data.suggestions ? (
                <>
                  <div>Instructions</div>
                  {data.suggestions.length > 0
                    ? data.suggestions.map((suggestion, i) => <div>{`${i + 1}. ${suggestion.name[0].content}`}</div>)
                    : null}
                </>
              ) : null}
            </div>

            <div>
              <MediaOptions
                theme={props.theme}
                message={message}
                setMessage={setMessage}
                fileObj={fileObj}
                setFileObj={setFileObj}
                setUploadProcessing={setUploadProcessing}
              />
            </div>

            <div>
              <UserForm
                theme={props.theme}
                message={message}
                setMessage={setMessage}
                user={user}
                setUser={setUser}
                createCollaborator={createCollaborator}
                showMediaHandle={props.showMediaHandle}
              />
            </div>
          </div>
        )
      ) : (
        <div style={{ minHeight: '70vh', position: 'relative' }}>
          <div style={{ position: 'absolute', top: '50%', left: '50%' }}>
            <Progress variant='circular' />
          </div>
        </div>
      )}
    </div>
  );
};

export default withRouter(Content);
