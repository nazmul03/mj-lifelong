import React from 'react';

import LifelongLogo from '../../assets/images/lifelong-logo.png';

const Header = (props) => {
  return (
    <div
      style={{
        backgroundColor: props.theme.backgroundColor,
        position: 'sticky',
        boxShadow: '0px 5px 5px #ddd',
        width: '100%'
      }}
    >
      <div style={{ padding: '1rem' }}>
        <img src={props.logo ? props.logo : LifelongLogo} alt='Logo' height='50' />
      </div>
    </div>
  );
};

export default Header;
