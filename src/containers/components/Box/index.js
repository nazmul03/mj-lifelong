import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  box: {
    cursor: 'pointer',
    // background: 'linear-gradient(45deg, #1D3DED, #004e92)',
    backgroundColor: (props) => props.backgroundColor || '#1D3DED',
    border: (props) => props.border || '0',
    color: (props) => props.color || '#fff',
    borderRadius: (props) => props.borderRadius || '10px',
    width: (props) => props.width || 'fit-content',
    height: (props) => props.height || 'fit-content',
    padding: (props) => props.padding || '8px 16px',
    margin: (props) => props.margin || '0',
    fontSize: (props) => props.fontSize || '1rem',
    boxShadow: (props) => props.boxShadow || '0 6px 5px 5px #ddd',
    transition: 'all 0.4s',
    '&:hover': {
      // background: 'linear-gradient(45deg, #004e92, #1D3DED)',
      backgroundColor: (props) => props.hoverBackgroundColor || '#1D3DED',
      color: (props) => props.hoverColor || '#fff',
      boxShadow: (props) => props.hoverBoxShadow || '0 3px 5px 1px #ddd'
    },
    '&:hover, &:focus': {
      outline: 'none'
    }
  }
}));
const Box = (props) => {
  const classes = useStyles({
    backgroundColor: props.backgroundColor || (props.theme && props.theme.boxBackgroundColor) || '#1d3ded',
    border: props.border,
    color: props.color || props.theme.boxTextColor || '#fff',
    borderRadius: props.borderRadius,
    width: props.width,
    height: props.height,
    padding: props.padding,
    margin: props.margin,
    fontSize: props.fontSize,
    boxShadow: props.boxShadow,
    hoverBackgroundColor: props.hoverBackgroundColor || (props.theme && props.theme.boxBackgroundColor) || '#1d3ded',
    hoverColor: props.hoverColor || props.theme.boxTextColor || '#fff',
    hoverBoxShadow: props.hoverBoxShadow
  });

  return (
    <button onClick={props.does} className={classes.box}>
      {props.says}
    </button>
  );
};

export default Box;
