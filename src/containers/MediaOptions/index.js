import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { isBrowser, isMobile } from 'react-device-detect';

import SingleSelectedFile from '../components/SingleSelectedFile';
import FileUploader from '../components/FileUploader';
import WebcamComponent from '../components/WebcamComponent';
import VoicerecorderComponent from '../components/VoicerecorderComponent';
import DragUploadComponent from '../components/DragUploadComponent';
import CustomModal from '../components/CustomModal';
import Box from '../components/Box';

import videoIcon from '../../assets/images/videoIcon.png';
import audioIcon from '../../assets/images/audioIcon.png';
import mediaIcon from '../../assets/images/mediaIcon.png';
import VideocamIcon from '@material-ui/icons/Videocam';
import SpeakerIcon from '@material-ui/icons/Speaker';
import PermMediaIcon from '@material-ui/icons/PermMedia';
import BackupIcon from '@material-ui/icons/Backup';

const useStyles = makeStyles(() => ({
  uploadAreaStyle: {
    border: (theme) => `2px dashed ${theme.bodyTextColor}`,
    width: '100%',
    backgroundColor: (theme) => theme && theme.mediaBoxBackgroundColor,
    position: 'relative',
    borderRadius: '5px',
    cursor: 'pointer',
    boxShadow: '0 6px 5px 5px #ddd',
    transition: 'all 0.4s',
    '&:hover': {
      boxShadow: '0 3px 5px 1px #ddd'
    }
  },
  fileUpload: {
    cursor: 'pointer',
    backgroundColor: (theme) => theme.boxBackgroundColor,
    border: '0',
    color: (theme) => theme.boxTextColor,
    borderRadius: '10px',
    padding: '8px 16px',
    margin: '0',
    fontSize: '1rem',
    boxShadow: '0 3px 5px 5px #ddd',
    transition: 'all 0.6s',
    textAlign: 'center',
    width: '8rem',
    height: '8rem',
    '&:hover, &:focus': {
      outline: 'none'
    }
  },
  iconFont: {
    fontSize: '4rem'
  }
}));

const UploadArea = ({ theme, handleClick, uploadAreaStyle }) => {
  return (
    <div className={uploadAreaStyle} onClick={handleClick}>
      <div
        style={{
          height: 'fit-content',
          width: 'fit-content',
          margin: '1rem auto',
          textAlign: 'center'
        }}
      >
        {/* <img src={MediaIcon} alt='Media Icon' height='70' width='120' /> */}
        <BackupIcon style={{ fontSize: '80px', color: theme && theme.headerTextColor }} />
        <div style={{ fontSize: '0.8rem', color: theme && theme.bodyTextColor }}>
          Supports MP4, MKV, FLV, MPEG, 3GP formats
        </div>
        <div style={{ fontSize: '1.4rem', color: theme && theme.bodyTextColor, fontWeight: '500' }}>
          <span style={{ color: theme && theme.headerTextColor, fontWeight: '500' }}>Record</span>
          <span> or upload media</span>
        </div>
      </div>
    </div>
  );
};

const MediaOptions = (props) => {
  const classes = useStyles(props && props.theme);
  const [uploading, setUploading] = React.useState([]);
  // const [mediaAvailable, setMediaAvailable] = React.useState(false);
  const [openCam, setOpenCam] = React.useState(false);
  const [openRecorder, setOpenRecorder] = React.useState(false);
  const [openMediaOptions, setOpenMediaOptions] = React.useState(false);

  // React.useEffect(() => {
  //   navigator.getUserMedia =
  //     navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  //   if (navigator.getUserMedia) {
  //     setMediaAvailable(true);
  //   } else {
  //     setMediaAvailable(false);
  //   }
  // }, []);

  const selectFile = (files) => {
    Object.values(files).map((file) => {
      console.log(file);
      setUploading([...uploading, file.name]);

      file.url = window.URL.createObjectURL(file);

      let formData = new FormData();
      formData.append('file', file);
      props.setUploadProcessing(true);
      axios
        .post('https://api.miljulapp.com/api/v1/common/fileUpload', formData)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            file.media_url = response.data.url;
            setUploading([]);
            props.setUploadProcessing(false);
          } else {
            props.setMessage('Error in file upload process');
            props.setUploadProcessing(false);
            setUploading([]);
          }
        })
        .catch((error) => {
          console.log(error);
          props.setMessage('Error in file ppload process');
          setUploading([]);
          props.setUploadProcessing(false);
        });

      return props.setFileObj((initialFiles) => {
        return [...initialFiles, file];
      });
    });
  };

  const removeFromSelectedFiles = (removed) => {
    props.setFileObj(props.fileObj.filter((file) => file.name !== removed.name));
  };

  const findFileType = (fileType) => fileType.substr(0, fileType.indexOf('/'));

  return (
    <div style={{ padding: '1rem 0' }}>
      {props.fileObj.length > 0 ? (
        <div style={{ height: '10rem', width: '100%', overflow: 'auto' }}>
          {props.fileObj.map((file, i) => {
            return (
              <SingleSelectedFile
                src={
                  findFileType(file.type) === 'image'
                    ? file.url
                    : findFileType(file.type) === 'video'
                    ? videoIcon
                    : findFileType(file.type) === 'audio'
                    ? audioIcon
                    : mediaIcon
                }
                name={file.name}
                uploading={uploading}
                removeFromSelectedFiles={() => removeFromSelectedFiles(file)}
              />
            );
          })}
        </div>
      ) : null}

      <DragUploadComponent selectFile={selectFile}>
        {isBrowser ? (
          <>
            <UploadArea
              uploadAreaStyle={classes.uploadAreaStyle}
              theme={props.theme}
              handleClick={() => setOpenMediaOptions(true)}
            />
            <CustomModal
              open={openMediaOptions}
              setOpen={() => setOpenMediaOptions(false)}
              theme={props.theme}
              padding='1rem 2rem'
            >
              <div className='row' style={{ width: 'fit-content' }}>
                <div className='col-12 col-sm-auto p-3 text-center my-auto' style={{ width: 'fit-content' }}>
                  <VoicerecorderComponent
                    uploading={uploading}
                    setUploading={setUploading}
                    setMessage={props.setMessage}
                    setFileObj={props.setFileObj}
                    openRecorder={openRecorder}
                    setOpenRecorder={setOpenRecorder}
                    setOpenMediaOptions={setOpenMediaOptions}
                    theme={props.theme}
                    setUploadProcessing={props.setUploadProcessing}
                  >
                    <Box
                      says={
                        <div>
                          <SpeakerIcon className={classes.iconFont} />
                          <div>Record Audio</div>
                        </div>
                      }
                      does={() => setOpenRecorder(true)}
                      width='8rem'
                      height='8rem'
                      theme={props.theme}
                    />
                  </VoicerecorderComponent>
                </div>

                <div className='col-12 col-sm-auto p-3 text-center my-auto' style={{ width: 'fit-content' }}>
                  <WebcamComponent
                    uploading={uploading}
                    setUploading={setUploading}
                    setMessage={props.setMessage}
                    setFileObj={props.setFileObj}
                    openCam={openCam}
                    setOpenCam={setOpenCam}
                    setOpenMediaOptions={setOpenMediaOptions}
                    theme={props.theme}
                    setUploadProcessing={props.setUploadProcessing}
                  >
                    <Box
                      says={
                        <div>
                          <VideocamIcon style={{ fontSize: '6rem' }} />
                          <div>Record Video</div>
                        </div>
                      }
                      does={() => setOpenCam(!openCam)}
                      width='10rem'
                      height='10rem'
                      theme={props.theme}
                    />
                  </WebcamComponent>
                </div>

                <div className='col-12 col-sm-auto p-3 text-center my-auto' style={{ width: 'fit-content' }}>
                  <FileUploader
                    selectFile={selectFile}
                    openMediaOptions={openMediaOptions}
                    setOpenMediaOptions={setOpenMediaOptions}
                  >
                    <div className={classes.fileUpload}>
                      <PermMediaIcon className={classes.iconFont} />
                      <div>Upload Media</div>
                    </div>
                  </FileUploader>
                </div>
              </div>
            </CustomModal>
          </>
        ) : isMobile ? (
          <FileUploader selectFile={selectFile}>
            <UploadArea uploadAreaStyle={classes.uploadAreaStyle} uploading={uploading} theme={props.theme} />
          </FileUploader>
        ) : (
          <FileUploader selectFile={selectFile}>
            <UploadArea uploadAreaStyle={classes.uploadAreaStyle} uploading={uploading} theme={props.theme} />
          </FileUploader>
        )}
      </DragUploadComponent>
    </div>
  );
};

export default MediaOptions;
